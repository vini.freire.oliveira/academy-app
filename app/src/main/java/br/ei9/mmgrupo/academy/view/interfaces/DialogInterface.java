package br.ei9.mmgrupo.academy.view.interfaces;


import br.ei9.mmgrupo.academy.model.realm.DailyRunningDto;
import br.ei9.mmgrupo.academy.model.realm.DailyTrainingDto;
import br.ei9.mmgrupo.academy.model.realm.GoalsDto;
import br.ei9.mmgrupo.academy.model.realm.MeasuresDto;
import br.ei9.mmgrupo.academy.view.dialog.GoalsDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.MeasuresListDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.RunningDailyDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.TrainingConfirmationDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.TrainingDialogFragment;

/**
 * Created by vini on 30/01/18.
 */

public interface DialogInterface {
    void clickOnConfirmation(TrainingConfirmationDialogFragment oldFragment);
    void clickOnConfirmation(TrainingConfirmationDialogFragment oldFragment, TrainingDialogFragment trainingDialogFragment, DailyTrainingDto dailyTrainingDto);
    void clickOnConfirmation(RunningDailyDialogFragment oldFragment, DailyRunningDto dailyRunningDto);
    void clickOnConfirmation(GoalsDialogFragment oldFragment, GoalsDto dailyRunningDto);
    void clickOnConfirmation(MeasuresListDialogFragment oldFragment, MeasuresDto measuresDto);
}
