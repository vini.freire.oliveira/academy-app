package br.ei9.mmgrupo.academy.model.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vini on 13/01/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MobileProfileResponse implements Serializable{

    private int status;
    private ProfileContent message;

    public MobileProfileResponse() {
    }

    public MobileProfileResponse(int status, ProfileContent message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ProfileContent getMessage() {
        return message;
    }

    public void setMessage(ProfileContent message) {
        this.message = message;
    }

    public class ProfileContent implements Serializable{

        private ProfileMobile profile;
        private Gym gym;
        private PlanProfile plan_client;


        public ProfileContent() {
        }

        public ProfileContent(ProfileMobile profile, Gym gym, PlanProfile plan_client) {
            this.profile = profile;
            this.gym = gym;
            this.plan_client = plan_client;
        }

        public ProfileMobile getProfile() {
            return profile;
        }

        public void setProfile(ProfileMobile profile) {
            this.profile = profile;
        }

        public Gym getGym() {
            return gym;
        }

        public void setGym(Gym gym) {
            this.gym = gym;
        }

        public PlanProfile getPlan_client() {
            return plan_client;
        }

        public void setPlan_client(PlanProfile plan_client) {
            this.plan_client = plan_client;
        }

        public class ProfileMobile implements Serializable{
            protected Integer id;
            protected Integer user_id;
            protected Integer gym_id;
            protected Integer plan_client_id;
            protected String address;
            protected String phone;
            protected String name;
            protected String cpf;
            protected Date created_at;
            protected Date updated_at;

            public ProfileMobile() {
            }

            public ProfileMobile(Integer id, Integer user_id, Integer gym_id, Integer plan_client_id, String address, String phone, String name, String cpf, Date created_at, Date updated_at) {
                this.id = id;
                this.user_id = user_id;
                this.gym_id = gym_id;
                this.plan_client_id = plan_client_id;
                this.address = address;
                this.phone = phone;
                this.name = name;
                this.cpf = cpf;
                this.created_at = created_at;
                this.updated_at = updated_at;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCpf() {
                return cpf;
            }

            public void setCpf(String cpf) {
                this.cpf = cpf;
            }

            public Date getCreated_at() {
                return created_at;
            }

            public void setCreated_at(Date created_at) {
                this.created_at = created_at;
            }

            public Date getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(Date updated_at) {
                this.updated_at = updated_at;
            }
        }

        public class Gym implements Serializable{
            protected Integer id;
            protected Integer manager_id;
            protected Integer plan_manager_id;
            protected String cnpj;
            protected String name;
            protected String address;
            protected String cep;
            protected String city;
            protected String state;
            protected String phone;
            protected String cellphone;
            protected String lat;
            protected String lon;
            protected Date due_date;
            protected Date created_at;
            protected Date updated_at;

            public Gym() {
            }

            public Gym(Integer id, Integer manager_id, Integer plan_manager_id, String cnpj, String name, String address, String cep, String city, String state, String phone, String cellphone, String lat,
                    String lon, Date due_date, Date created_at, Date updated_at) {
                this.id = id;
                this.manager_id = manager_id;
                this.plan_manager_id = plan_manager_id;
                this.cnpj = cnpj;
                this.name = name;
                this.address = address;
                this.cep = cep;
                this.city = city;
                this.state = state;
                this.phone = phone;
                this.cellphone = cellphone;
                this.lat = lat;
                this.lon = lon;
                this.due_date = due_date;
                this.created_at = created_at;
                this.updated_at = updated_at;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getManager_id() {
                return manager_id;
            }

            public void setManager_id(Integer manager_id) {
                this.manager_id = manager_id;
            }

            public Integer getPlan_manager_id() {
                return plan_manager_id;
            }

            public void setPlan_manager_id(Integer plan_manager_id) {
                this.plan_manager_id = plan_manager_id;
            }

            public String getCnpj() {
                return cnpj;
            }

            public void setCnpj(String cnpj) {
                this.cnpj = cnpj;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCep() {
                return cep;
            }

            public void setCep(String cep) {
                this.cep = cep;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getCellphone() {
                return cellphone;
            }

            public void setCellphone(String cellphone) {
                this.cellphone = cellphone;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public Date getCreated_at() {
                return created_at;
            }

            public void setCreated_at(Date created_at) {
                this.created_at = created_at;
            }

            public Date getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(Date updated_at) {
                this.updated_at = updated_at;
            }
        }

        public class PlanProfile implements Serializable{
            protected Integer id;
            protected Integer gym_id;
            protected String name;
            protected Float price;
            protected Date created_at;
            protected Date updated_at;

            public PlanProfile() {
            }

            public PlanProfile(Integer id, Integer gym_id, String name, Float price, Date created_at, Date updated_at) {
                this.id = id;
                this.gym_id = gym_id;
                this.name = name;
                this.price = price;
                this.created_at = created_at;
                this.updated_at = updated_at;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Float getPrice() {
                return price;
            }

            public void setPrice(Float price) {
                this.price = price;
            }

            public Date getCreated_at() {
                return created_at;
            }

            public void setCreated_at(Date created_at) {
                this.created_at = created_at;
            }

            public Date getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(Date updated_at) {
                this.updated_at = updated_at;
            }
        }

    }
}
