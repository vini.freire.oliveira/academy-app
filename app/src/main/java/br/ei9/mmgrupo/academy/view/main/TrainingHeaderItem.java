package br.ei9.mmgrupo.academy.view.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vini on 26/01/18.
 */

public class TrainingHeaderItem implements Serializable {

    protected String group;
    protected String turn;
    protected List<TrainingChildItem> itens = new ArrayList<>();


    public TrainingHeaderItem(String group, String turn) {
        this.group = group;
        this.turn = turn;
    }

    public TrainingHeaderItem() {
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getTurn() {
        return turn;
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }

    public void add(TrainingChildItem item){
        this.itens.add(item);
    }

    public List<TrainingChildItem> getItens() {
        return itens;
    }
}
