package br.ei9.mmgrupo.academy.view.main;


import android.app.AlertDialog;
import android.util.Log;
import android.widget.ListView;


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.PaymentDto;
import br.ei9.mmgrupo.academy.model.realm.UserDto;
import br.ei9.mmgrupo.academy.model.service.ProfilePaymentsResponse;
import br.ei9.mmgrupo.academy.rest.RestClient;
import br.ei9.mmgrupo.academy.view.Utils;

/**
 * Created by vini on 11/01/18.
 */

@EFragment(R.layout.fragment_payment)
public class FragmentPayments extends MyFragment {

    @RestService
    RestClient rest;

    @Bean
    PaymentsAdapter adapter;

    @ViewById(R.id.payment_list)
    ListView list;

    AlertDialog progressDialog;

    @AfterViews
    public void onCreate(){
        list.setAdapter(adapter);
        if(isReadyForRequest()){
            progressDialog = createProgressDialog();
            updateList();
        }else{
            populateOldList();
        }
    }

    @Background
    protected void updateList(){
        rest.setHeader("Content-Type", "application/json");
        if(Utils.acess_token.equals("")){
            List<UserDto> user = (List<UserDto>) RealmObjectManipulate.getAll(UserDto.class);
            rest.setHeader("uid",user.get(0).getEmail());
            rest.setHeader("client",user.get(0).getClient());
            rest.setHeader("access-token",user.get(0).getToken());
        }else{
            rest.setHeader("uid",Utils.uid);
            rest.setHeader("client",Utils.client);
            rest.setHeader("access-token",Utils.acess_token);
        }
        rest.getProfilePayments();
        verifyRequest(Utils.responseList);
    }

    protected void verifyRequest(String response){
        Log.d("#",response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getInt("status") == 200){
                List<PaymentsItem> objects = getListPayments(response);
                for(PaymentsItem item :objects){
                    RealmObjectManipulate.save(new PaymentDto(item.getId(),item.getDate(),item.getValue()));
                }
                adapter.setList(objects);
                notifyDataSetChanged();
                finishProgressDialog(progressDialog);
            }else{
                finishProgressDialog(progressDialog);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public List<PaymentsItem> getListPayments(String list){
        String json = list.replace("\"status\":200,","");
        JSONObject jsonObject;
        JSONArray payments;
        List<PaymentsItem> objects = new ArrayList<>();
        try {
            jsonObject = new JSONObject(json);
            payments = jsonObject.getJSONArray("message");
            for(int i=0;i<payments.length();i++){
                Date date = convertToDate(payments.getJSONObject(i).get("payment_day").toString());
                Integer id = Integer.parseInt(payments.getJSONObject(i).get("id").toString());
                Float price = Float.parseFloat(payments.getJSONObject(i).get("price").toString());
                objects.add(new PaymentsItem(id,date,price));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return objects;
    }

    protected void populateOldList(){
        if(RealmObjectManipulate.getAll(PaymentDto.class).size() > 0){
            List<PaymentDto> payments = (List<PaymentDto>) RealmObjectManipulate.getAll(PaymentDto.class);
            List<PaymentsItem> objects = new ArrayList<>();
            for(PaymentDto x : payments){
                objects.add(new PaymentsItem(x.getDate(),x.getValue()));
            }
            adapter.setList(objects);
            notifyDataSetChanged();
        }
    }

    @UiThread
    public void notifyDataSetChanged(){
        adapter.notifyDataSetChanged();
    }
}
