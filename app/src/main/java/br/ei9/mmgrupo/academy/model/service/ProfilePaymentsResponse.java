package br.ei9.mmgrupo.academy.model.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by vini on 24/01/18.
 */

public class ProfilePaymentsResponse implements Serializable {

    private int status;
    private List<PaymentsContent> message = new ArrayList<>();

    public ProfilePaymentsResponse(){
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<PaymentsContent> getMessage() {
        return message;
    }

    public void setMessage(PaymentsContent item) {
        this.message.add(item);
    }

    public class PaymentsContent implements Serializable{
        protected Integer id;
        protected Integer client_id;
        protected Float price;
        protected Date payment_day;
        protected Date created_at;
        protected Date updated_at;

        public PaymentsContent() {
        }

        public PaymentsContent(Integer id, Float price, Date payment_day) {
            this.id = id;
            this.price = price;
            this.payment_day = payment_day;
        }

        public PaymentsContent(Integer id, Integer client_id, Float price, Date payment_day, Date created_at, Date updated_at) {
            this.id = id;
            this.client_id = client_id;
            this.price = price;
            this.payment_day = payment_day;
            this.created_at = created_at;
            this.updated_at = updated_at;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getClient_id() {
            return client_id;
        }

        public void setClient_id(Integer client_id) {
            this.client_id = client_id;
        }

        public Float getPrice() {
            return price;
        }

        public void setPrice(Float price) {
            this.price = price;
        }

        public Date getPayment_day() {
            return payment_day;
        }

        public void setPayment_day(Date payment_day) {
            this.payment_day = payment_day;
        }

        public Date getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Date created_at) {
            this.created_at = created_at;
        }

        public Date getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Date updated_at) {
            this.updated_at = updated_at;
        }
    }
}
