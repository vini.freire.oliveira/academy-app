package br.ei9.mmgrupo.academy.rest;

import android.util.Log;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import br.ei9.mmgrupo.academy.view.Utils;


/**
 * Created by vinif_000 on 12/08/2017.
 */
public class InterceptorRest implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

        Log.d("#", "----------------------------REST--------------------");
        Log.d("#", request.getMethod().toString());
        Log.d("#", new String(body).toString());
        Log.d("#", "----------------------------REST--------------------");

        ClientHttpResponse response = execution.execute(request, body);

        Log.d("#", "----------------------------HEADER--------------------");
        Log.d("#", new String(response.getHeaders().toString()).toString());
        Log.d("#", "----------------------------HEADER--------------------");

        String responseString = stringOf(response.getBody());
        Log.d("RESPONSE", responseString);
        Utils.responseList = responseString;


        if(response.getHeaders().get("access-token") != null){
            if(response.getHeaders().get("access-token").get(0) !=null){
                if(!response.getHeaders().get("access-token").get(0).equals("")){
                    Utils.acess_token = response.getHeaders().get("access-token").get(0);
                    Utils.uid = response.getHeaders().get("uid").get(0);
                    Utils.client = response.getHeaders().get("client").get(0);
                }
            }
        }

        return execution.execute(request, body);
    }

    public static String stringOf(InputStream inputStream) {

        inputStream.mark(Integer.MAX_VALUE);
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder strBuilder = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null)
                strBuilder.append(line);
        } catch (IOException ignored) {}
        try {
            inputStream.reset();
        } catch (IOException ignored) {}
        return strBuilder.toString();
    }
}