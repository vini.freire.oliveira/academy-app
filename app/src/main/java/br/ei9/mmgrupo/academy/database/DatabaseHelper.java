package br.ei9.mmgrupo.academy.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import br.ei9.mmgrupo.academy.R;

/**
 * Created by vini on 17/12/17.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    // name of the database file for your application ­­ change to something      
    //  appropriate for your app    
    private static final String DATABASE_NAME = "";

    // any time you make changes to your database objects, you may have to     
    // increase the database version      
    private static final int DATABASE_VERSION = 1;

    Context context;


    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION,
                R.raw.ormlite_config );

        this.context = context;

    }

    /**     
     * This is called when the database is first created. Usually you should      
     * call createTable statements here to create the tables that will store 
     * your data.
     */
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {

            Log.i(DatabaseHelper.class.getName(), "onCreate");

            // TableUtils.createTable(connectionSource,NomeTabela.class );

            setup();

            // this method was create to make the code as correct after create the tables delete this.
            throw new SQLException();

        }catch (SQLException e){
            Log.e(DatabaseHelper.class.getName(),"Can´t crate a database",e);
            throw new RuntimeException(e);

        }

    }


    /**      
     * This is called when your application is upgraded and it has a higher       
     * version number. This allows you to adjust the various data to match the      
     * new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            // TableUtils.dropTable(connectionSource,NomeTabela.class,true);

            // After we've droped the old databases, we'll create the new ones.
            onCreate(database,connectionSource);

            // this method was create to make the code as correct after create the tables delete this.
            throw new SQLException();

        }catch (SQLException e){
            Log.e(DatabaseHelper.class.getName(),"Can´t drop the database",e);
            throw new RuntimeException(e);
        }

    }



    public void clearUserData(){

        try {

            //   TableUtils.clearTable(connectionSource, NomeTabela.class);

            // this method was create to make the code as correct after create the tables delete this.
            throw new SQLException();

        }catch (SQLException e){
            e.printStackTrace();
        }

    }


    private void setup(){
        // Locality para a criação de dados "pré-carregados"
    }

}
