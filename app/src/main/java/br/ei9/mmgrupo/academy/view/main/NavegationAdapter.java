package br.ei9.mmgrupo.academy.view.main;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

import br.ei9.mmgrupo.academy.R;

/**
 * Created by vini on 11/01/18.
 */

@EBean
public class NavegationAdapter extends BaseAdapter {

    List<NavegationItem> objects;

    @RootContext
    Context context;

    @AfterInject
    public void inject(){
        this.objects = populate();
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public NavegationItem getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NavegationItemView itemView;

        if(convertView == null){
            itemView = NavegationItemView_.build(context);
        }else{
            itemView = (NavegationItemView) convertView;
        }

        itemView.bind(objects.get(position));

        return itemView;
    }

    private List<NavegationItem> populate(){
        List<NavegationItem> menu = new ArrayList<>();
        NavegationItem dashboard = new NavegationItem(R.drawable.dashboard,context.getResources().getString(R.string.dashoard));
        NavegationItem card = new NavegationItem(R.drawable.minha_ficha,context.getResources().getString(R.string.my_card));
        NavegationItem measures = new NavegationItem(R.drawable.medidas,context.getResources().getString(R.string.my_measures));
        NavegationItem payments = new NavegationItem(R.drawable.pagamentos,context.getResources().getString(R.string.my_payments));

        menu.add(dashboard);menu.add(card);menu.add(measures);menu.add(payments);
        return menu;
    }
}
