package br.ei9.mmgrupo.academy.view.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;


import java.util.List;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.GymDto;
import br.ei9.mmgrupo.academy.model.realm.ProfileDto;


@EActivity(R.layout.activity_main)
public class MainActivity extends MyActivity {

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @ViewById(R.id.left_drawer)
    ListView listView;

    @ViewById(R.id.user_name_main)
    TextView userName;

    @ViewById(R.id.user_academy_name)
    TextView userAcademyName;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    ActionBarDrawerToggle mDrawerToggle;

    public static int currentFragment = 0;

    @Bean
    NavegationAdapter adapter;

    @AfterViews
    public void onCreate(){
        setupToolbar();
        listView.setAdapter(adapter);
        startCurrentFragment(currentFragment);
        setupDrawerToggle();
        setUpView();

    }

    @ItemClick(R.id.left_drawer)
    public void itemClick(int position){
        startCurrentFragment(position);
    }

    protected void startCurrentFragment(int position){

        Fragment fragment = new FragmentDashboard();
        String title = getResources().getString(R.string.dashoard);
        switch (position){
            case 0:
                fragment = new FragmentDashboard_();
                currentFragment = 0;
                title = getResources().getString(R.string.dashoard);
                break;
            case 1:
                fragment = new FragmentCard_();
                currentFragment = 1;
                title = getResources().getString(R.string.my_card);
                break;
            case 2:
                fragment = new FragmentMeasures_();
                currentFragment = 2;
                title = getResources().getString(R.string.my_measures);
                break;
            case 3:
                fragment = new FragmentPayments_();
                currentFragment = 3;
                title = getResources().getString(R.string.my_payments);
                break;
            default:
                break;
        }

        if(fragment != null){
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            listView.setItemChecked(position, true);
            listView.setSelection(position);
            setupToolbarTitle(title);
            drawerLayout.closeDrawers();

        }else{
            Log.e("MainActivity", "Error in creating fragment");
        }

    }

    protected void setUpView(){
        List<ProfileDto> profiles = (List<ProfileDto>) RealmObjectManipulate.getAll(ProfileDto.class);
        userName.setText(profiles.get(0).getName());
        List<GymDto> gyms = (List<GymDto>) RealmObjectManipulate.getAll(GymDto.class);
        userAcademyName.setText(gyms.get(0).getName());
    }

    protected void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    protected void setupDrawerToggle(){
        mDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.app_name,R.string.app_name);
        mDrawerToggle.syncState();
    }

    protected void setupToolbarTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
}
