package br.ei9.mmgrupo.academy.model.realm;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by vini on 30/01/18.
 */

@RealmClass
public class DailyTrainingDto extends RealmObject {

    @PrimaryKey
    protected Integer id;

    protected Integer trainingID;
    protected Date startAt;
    protected Date endedAt;
    protected Float exerciseDone;


    public DailyTrainingDto() {
    }

    public DailyTrainingDto(Integer id, Integer trainingID, Date startAt, Date endedAt, Float exerciseDone) {
        this.id = id;
        this.trainingID = trainingID;
        this.startAt = startAt;
        this.endedAt = endedAt;
        this.exerciseDone = exerciseDone;
    }

    public DailyTrainingDto(Integer id, Integer trainingID, Date startAt) {
        this.id = id;
        this.trainingID = trainingID;
        this.startAt = startAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTrainingID() {
        return trainingID;
    }

    public void setTrainingID(Integer trainingID) {
        this.trainingID = trainingID;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Date endedAt) {
        this.endedAt = endedAt;
    }

    public Float getExerciseDone() {
        return exerciseDone;
    }

    public void setExerciseDone(Float exerciseDone) {
        this.exerciseDone = exerciseDone;
    }

}
