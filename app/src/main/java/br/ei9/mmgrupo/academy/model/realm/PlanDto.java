package br.ei9.mmgrupo.academy.model.realm;

import java.util.Date;

import br.ei9.mmgrupo.academy.model.service.MobileProfileResponse;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by vini on 13/01/18.
 */

@RealmClass
public class PlanDto extends RealmObject {

    @PrimaryKey
    protected Integer id;

    protected String name;
    protected Float price;
    protected Date createdAt;
    protected Date updatedAt;

    public PlanDto() {
    }

    public PlanDto(Integer id, String name, Float price, Date createdAt, Date updatedAt) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public PlanDto(MobileProfileResponse.ProfileContent.PlanProfile plan) {
        this.id = plan.getId();
        this.name = plan.getName();
        this.price = plan.getPrice();
        this.createdAt = plan.getCreated_at();
        this.updatedAt = plan.getUpdated_at();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
