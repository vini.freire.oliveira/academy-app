package br.ei9.mmgrupo.academy.view.main;

import java.io.Serializable;

/**
 * Created by vini on 11/01/18.
 */

public class NavegationItem implements Serializable {

    protected int image;
    protected String item;

    public NavegationItem() {
    }

    public NavegationItem(String item) {
        this.item = item;
    }

    public NavegationItem(int image, String item) {
        this.image = image;
        this.item = item;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
