package br.ei9.mmgrupo.academy.view.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Date;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.DailyRunningDto;
import br.ei9.mmgrupo.academy.model.realm.DailyTrainingDto;
import br.ei9.mmgrupo.academy.view.interfaces.DialogInterface;

/**
 * Created by vini on 01/02/18.
 */

public class RunningDailyDialogFragment extends DialogFragment{

    protected EditText kms;
    protected EditText time;
    protected EditText calories;
    protected DialogInterface dialogInterface;
    protected RunningDailyDialogFragment oldFragment;

    public void setDialogInterface(DialogInterface dialogInterface, RunningDailyDialogFragment oldFragment) {
        this.dialogInterface = dialogInterface;
        this.oldFragment = oldFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyCustomDialogInfo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dialog_fragment_running, container, false);

        kms = (EditText) v.findViewById(R.id.km_spend);
        time = (EditText) v.findViewById(R.id.time_spend);
        calories = (EditText) v.findViewById(R.id.calories_spend);

        // Watch for button clicks.
        Button buttonConfirm = (Button)v.findViewById(R.id.dialog_button_confirm);
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(kms.getText().toString().length() == 0 && calories.getText().toString().length() == 0){
                }else{
                    String spendTime = "30";
                    if(time.getText().toString().length() > 0){
                        spendTime = time.getText().toString();
                    }

                    DailyRunningDto runningDto;
                    if(RealmObjectManipulate.getAll(DailyTrainingDto.class).size() > 0){
                        runningDto = new DailyRunningDto(
                                RealmObjectManipulate.getAll(DailyTrainingDto.class).size()+1,
                                Float.parseFloat(kms.getText().toString()),
                                Float.parseFloat(calories.getText().toString()),
                                Float.parseFloat(spendTime),
                                new Date());
                    }else{
                        runningDto = new DailyRunningDto(1,Float.parseFloat(kms.getText().toString()),
                                Float.parseFloat(calories.getText().toString()),Float.parseFloat(spendTime),new Date());
                    }

                    dialogInterface.clickOnConfirmation(oldFragment,runningDto);
                }
            }
        });

        Button buttonCancel = (Button)v.findViewById(R.id.dialog_button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return v;
    }
}
