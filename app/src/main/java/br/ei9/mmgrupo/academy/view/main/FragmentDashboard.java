package br.ei9.mmgrupo.academy.view.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.DailyRunningDto;
import br.ei9.mmgrupo.academy.model.realm.DailyTrainingDto;
import br.ei9.mmgrupo.academy.model.realm.GoalsDto;
import br.ei9.mmgrupo.academy.model.realm.MeasuresDto;
import br.ei9.mmgrupo.academy.view.dialog.GoalsDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.MeasuresDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.MeasuresListDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.MyDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.RunningDailyDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.TrainingConfirmationDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.TrainingDialogFragment;
import br.ei9.mmgrupo.academy.view.interfaces.DialogInterface;
import br.ei9.mmgrupo.academy.view.main.graph.FragmentChartLine;
import br.ei9.mmgrupo.academy.view.main.graph.FragmentChartLine_;

/**
 * Created by vini on 11/01/18.
 */

@EFragment(R.layout.fragment_dashboard)
public class FragmentDashboard extends MyFragment implements DialogInterface{

    protected FragmentTransaction ft;
    protected Fragment prev;
    protected FragmentChartLine fragmentChartLine;


    @AfterViews
    public void onCreate(){
        setUpProgressLine();
        createGraph();
    }

    public void setUpProgressLine(){

    }

    public void createGraph(){
        fragmentChartLine = new FragmentChartLine_();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.dashboard_graph, fragmentChartLine).commit();
    }

    int testeaux = 0;
    @Click(R.id.dashboard_settings)
    public void clickOnGraphSettings(){
        testeaux = (testeaux + 1)% 4;
        fragmentChartLine.updateGraph(testeaux);

    }

    @Click(R.id.dashboard_button_goals)
    public void clickOnbuttonGoals(){
        ft = getFragmentManager().beginTransaction();
        prev = getFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        List<GoalsDto> goals = (List<GoalsDto>) RealmObjectManipulate.getAll(GoalsDto.class);
        if(goals.size() > 0){

            GoalsDto lastGoals = goals.get(goals.size()-1);
            Date today = Calendar.getInstance().getTime();
            if(lastGoals.getDate().compareTo(today) <= 0){
                MyDialogFragment newFragment = MyDialogFragment.newInstance(1);
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String dategoal = formatter.format(lastGoals.getDate());
                newFragment.setContent("Já existe uma meta, para alcança-la você precisa chegar a " + lastGoals.getWeightGoal() + " quilos até o dia:  " + dategoal );
                newFragment.show(ft, "dialog");
            }else{
                GoalsDialogFragment newFragment = new GoalsDialogFragment();
                newFragment.setDialogInterface(this,newFragment);
                newFragment.show(ft, "dialog");
            }

        }else{
            GoalsDialogFragment newFragment = new GoalsDialogFragment();
            newFragment.setDialogInterface(this,newFragment);
            newFragment.show(ft, "dialog");
        }


    }

    @Click(R.id.dashboard_button_measures)
    public void clickOnbuttonMeasures(){
        ft = getFragmentManager().beginTransaction();
        prev = getFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        MeasuresListDialogFragment newFragment = new MeasuresListDialogFragment();
        newFragment.setDialogInterface(this,newFragment);
        newFragment.show(ft, "dialog");
    }

    @Click(R.id.dashboard_button_run)
    public void clickOnbuttonRun(){
        ft = getFragmentManager().beginTransaction();
        prev = getFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        RunningDailyDialogFragment newFragment = new RunningDailyDialogFragment();
        newFragment.setDialogInterface(this,newFragment);
        newFragment.show(ft, "dialog");
    }

    @Click(R.id.dashboard_button_start_training)
    public void buttonStartTraining(){
        ft = getFragmentManager().beginTransaction();
        prev = getFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        // Create and show the dialog.
        TrainingConfirmationDialogFragment newFragment = new TrainingConfirmationDialogFragment();
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy, hh:mm:ss");
        String today = formatter.format(new Date());
        newFragment.setContent(this, newFragment, "Seu treino irá iniciar no dia: " + today.split(",")[0] + " ás " + today.split(",")[1]);
        newFragment.show(ft, "dialog");
    }


    @Override
    public void clickOnConfirmation(TrainingConfirmationDialogFragment oldFragment) {
        ft = getFragmentManager().beginTransaction();
        prev = getFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.remove(oldFragment);

        // Create and show the dialog.
        TrainingDialogFragment newFragment = new TrainingDialogFragment();
        newFragment.setDialogInterface(newFragment,this);
        newFragment.show(ft, "dialog");

    }

    @Override
    public void clickOnConfirmation(TrainingConfirmationDialogFragment oldFragment, TrainingDialogFragment trainingDialogFragment, DailyTrainingDto dailyTrainingDto) {
        ft = getFragmentManager().beginTransaction();
        prev = getFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.remove(oldFragment);
        ft.remove(trainingDialogFragment);
        RealmObjectManipulate.save(dailyTrainingDto);

        MyDialogFragment newFragment = MyDialogFragment.newInstance(1);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy, hh:mm:ss");
        newFragment.setContent( "Treino salvo com sucesso\n\n\n"+
                "Data de início: " + formatter.format(dailyTrainingDto.getStartAt()) + "\n"+
                "Data de término: " + formatter.format(dailyTrainingDto.getEndedAt()) + "\n"+
                "Percentual de exercícios realizados: " + dailyTrainingDto.getExerciseDone() + "%\n");

        newFragment.show(ft, "dialog");
    }

    @Override
    public void clickOnConfirmation(RunningDailyDialogFragment oldFragment, DailyRunningDto dailyRunningDto) {
        ft = getFragmentManager().beginTransaction();
        prev = getFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.remove(oldFragment);
        RealmObjectManipulate.save(dailyRunningDto);

        MyDialogFragment newFragment = MyDialogFragment.newInstance(1);
        newFragment.setContent("Evento salvo com sucesso");
        newFragment.show(ft, "dialog");
    }

    @Override
    public void clickOnConfirmation(GoalsDialogFragment oldFragment, GoalsDto goalsDto) {
        ft = getFragmentManager().beginTransaction();
        prev = getFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.remove(oldFragment);
        RealmObjectManipulate.save(goalsDto);

        MyDialogFragment newFragment = MyDialogFragment.newInstance(1);
        newFragment.setContent("Meta salvo com sucesso");
        newFragment.show(ft, "dialog");
    }

    @Override
    public void clickOnConfirmation(MeasuresListDialogFragment oldFragment, MeasuresDto measure) {
        ft = getFragmentManager().beginTransaction();
        prev = getFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.remove(oldFragment);

        MeasuresDialogFragment newFragment = MeasuresDialogFragment.newInstance(8);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy, hh:mm:ss");
        String today = formatter.format(measure.getDate());
        newFragment.setTitle(getResources().getString(R.string.my_measures));

        String height = Float.toString(measure.getHeight());

        newFragment.setContent( "Data da medição: " + today + "\n"+
                "Altura: " + height.substring(0,1) + "." + height.substring(1,3) + " m\n"+
                "Peso: " + measure.getWeight() + " kg\n"+
                "Peitoral: " + measure.getPectoral() + " cm\n" +
                "Cintura: " + measure.getWaist() + " cm\n" +
                "Quadril: " + measure.getHip() + " cm\n"+
                "Braço direito: " + measure.getArmRight() + " cm\n" +
                "Braço esquerdo: " + measure.getArmLeft() +" cm\n"+
                "Coxa direita: " + measure.getThighRight() + " cm\n" +
                "Coxa esquerda: " + measure.getThighLeft() + " cm\n"+
                "Panturilha direita: " + measure.getCalfRight() + " cm\n"+
                "Panturilha esquerda: " + measure.getCalfLeft() + "cm\n");

        newFragment.show(ft, "dialog");
    }
}
