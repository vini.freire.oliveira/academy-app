package br.ei9.mmgrupo.academy.model.realm;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by vini on 06/01/18.
 */

@RealmClass
public class UserDto extends RealmObject{

    @PrimaryKey
    protected Integer id;

    protected String email;
    protected String client;
    protected String token;


    public UserDto() {
    }

    public UserDto(Integer id, String email, String client, String token) {
        this.id = id;
        this.email = email;
        this.client = client;
        this.token = token;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
