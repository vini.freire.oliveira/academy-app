package br.ei9.mmgrupo.academy.view.main.graph;

import android.content.res.Resources;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.animation.BounceInterpolator;
import android.widget.Toast;
import com.hrules.charter.CharterXMarkers;


import com.hrules.charter.CharterLine;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.Random;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.DailyRunningDto;
import br.ei9.mmgrupo.academy.model.realm.DailyTrainingDto;

/**
 * Created by vini on 20/01/18.
 */

@EFragment(R.layout.graph_charter_line)
public class FragmentChartLine extends Fragment {

    @ViewById(R.id.charter_line_XMarker)
    CharterXMarkers charterLineXMarkers;

    @ViewById(R.id.charter_line_with_XMarker)
    CharterLine charterLineWithXMarker;

    private static final int DEFAULT_ITEMS_COUNT = 15;
    private static final int DEFAULT_RANDOM_VALUE_MIN = 10;
    private static final int DEFAULT_RANDOM_VALUE_MAX = 100;

    private float[] values;


    @AfterViews
    public void onCreate(){

        Resources res = getResources();

        values = getPercentualExerciseDone();
        // charter_line
        charterLineWithXMarker.setValues(values);
        charterLineXMarkers.setSeparatorStrokeSize(2);
        charterLineXMarkers.setWidthCorrectionFromCharterLine(charterLineWithXMarker);
        charterLineXMarkers.setMarkersCount(DEFAULT_ITEMS_COUNT);
    }

    public void updateGraph(int value){
        switch (value){
            case 0:
                Snackbar.make(getView(),"exercícios realizados",Snackbar.LENGTH_SHORT).show();
                values = getPercentualExerciseDone();
                break;
            case 1:
                Snackbar.make(getView(),"corrida em km",Snackbar.LENGTH_SHORT).show();
                values = getRunningDone();
                break;
            case 2:
                Snackbar.make(getView(),"calorias",Snackbar.LENGTH_SHORT).show();
                values = getCaloriesSpend();
                break;
            case 3:
                Snackbar.make(getView(),"tempo",Snackbar.LENGTH_SHORT).show();
                values = getTimeSpend();
                break;
        }

        // charter_line
        charterLineWithXMarker.setValues(values);
        charterLineXMarkers.setSeparatorStrokeSize(2);
        charterLineXMarkers.setWidthCorrectionFromCharterLine(charterLineWithXMarker);
        charterLineXMarkers.setMarkersCount(DEFAULT_ITEMS_COUNT);
    }


    private float[] fillRandomValues(int length, int max, int min) {
        Random random = new Random();

        float[] newRandomValues = new float[length];
        for (int i = 0; i < newRandomValues.length; i++) {
            newRandomValues[i] = random.nextInt(max - min + 1) - min;
        }
        return newRandomValues;
    }

    protected float[] getPercentualExerciseDone(){
        List<DailyTrainingDto> dailyTrainingDtos = (List<DailyTrainingDto>) RealmObjectManipulate.getAll(DailyTrainingDto.class);
        float[] newValues;

        if(dailyTrainingDtos.size() > 15){
            newValues = new float[15];
            int x = 0;
            for(int i = dailyTrainingDtos.size() - 15; i < dailyTrainingDtos.size(); i++){
                newValues[x] = dailyTrainingDtos.get(i).getExerciseDone();
                x++;
            }

        }else{
            newValues = new float[dailyTrainingDtos.size()];
            for(int i =0; i<dailyTrainingDtos.size();i++){
                newValues[i] = dailyTrainingDtos.get(i).getExerciseDone();
            }
        }

        return newValues;

    }

    protected float[] getRunningDone(){
        List<DailyRunningDto> dailyRunnings = (List<DailyRunningDto>) RealmObjectManipulate.getAll(DailyRunningDto.class);
        float[] newValues;
        if(dailyRunnings.size() > DEFAULT_ITEMS_COUNT){
            newValues = new float[DEFAULT_ITEMS_COUNT];
            int x = 0;
            for(int i = dailyRunnings.size() - DEFAULT_ITEMS_COUNT; i < dailyRunnings.size(); i++){
                newValues[x] = dailyRunnings.get(i).getKm();
                x++;
            }

        }else{
            newValues = new float[dailyRunnings.size()];
            for(int i =0; i<dailyRunnings.size();i++){
                newValues[i] = dailyRunnings.get(i).getKm();
            }
        }

        return newValues;
    }

    protected float[] getCaloriesSpend(){
        List<DailyRunningDto> dailyRunnings = (List<DailyRunningDto>) RealmObjectManipulate.getAll(DailyRunningDto.class);
        float[] newValues;
        if(dailyRunnings.size() > DEFAULT_ITEMS_COUNT){
            newValues = new float[DEFAULT_ITEMS_COUNT];
            int x = 0;
            for(int i = dailyRunnings.size() - DEFAULT_ITEMS_COUNT; i < dailyRunnings.size(); i++){
                newValues[x] = dailyRunnings.get(i).getCalories();
                x++;
            }

        }else{
            newValues = new float[dailyRunnings.size()];
            for(int i =0; i<dailyRunnings.size();i++){
                newValues[i] = dailyRunnings.get(i).getCalories();
            }
        }

        return newValues;
    }

    protected float[] getTimeSpend(){
        // Aqui é bom deixar o mesmo valor de quilomentragem
        List<DailyRunningDto> dailyRunnings = (List<DailyRunningDto>) RealmObjectManipulate.getAll(DailyRunningDto.class);
        float[] newValues;
        if(dailyRunnings.size() > DEFAULT_ITEMS_COUNT){
            newValues = new float[DEFAULT_ITEMS_COUNT];
            int x = 0;
            for(int i = dailyRunnings.size() - DEFAULT_ITEMS_COUNT; i < dailyRunnings.size(); i++){
                newValues[x] = dailyRunnings.get(i).getTime();
                x++;
            }

        }else{
            newValues = new float[dailyRunnings.size()];
            for(int i =0; i<dailyRunnings.size();i++){
                newValues[i] = dailyRunnings.get(i).getTime();
            }
        }

        return newValues;
    }
}
