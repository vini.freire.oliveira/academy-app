package br.ei9.mmgrupo.academy.model.realm;

import java.util.Date;

import br.ei9.mmgrupo.academy.model.service.MobileProfileResponse;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by vini on 13/01/18.
 */

@RealmClass
public class GymDto extends RealmObject {

    @PrimaryKey
    protected Integer id;

    protected String cnpj;
    protected String name;
    protected String address;
    protected String cep;
    protected String city;
    protected String state;
    protected String phone;
    protected String cellphone;
    protected String lat;
    protected String lon;
    protected Date createdAt;
    protected Date updatedAt;

    public GymDto() {
    }

    public GymDto(Integer id, String cnpj, String name, String address, String cep, String city, String state, String phone, String cellphone, String lat, String lon, Date createdAt, Date updatedAt) {
        this.id = id;
        this.cnpj = cnpj;
        this.name = name;
        this.address = address;
        this.cep = cep;
        this.city = city;
        this.state = state;
        this.phone = phone;
        this.cellphone = cellphone;
        this.lat = lat;
        this.lon = lon;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public GymDto(MobileProfileResponse.ProfileContent.Gym gym) {
        this.id = gym.getId();
        this.cnpj = gym.getCnpj();
        this.name = gym.getName();
        this.address = gym.getAddress();
        this.cep = gym.getCep();
        this.city = gym.getCity();
        this.state = gym.getState();
        this.phone = gym.getPhone();
        this.cellphone = gym.getCellphone();
        this.lat = gym.getLat();
        this.lon = gym.getLon();
        this.createdAt = gym.getCreated_at();
        this.updatedAt = gym.getUpdated_at();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
