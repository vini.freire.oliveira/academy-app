package br.ei9.mmgrupo.academy.view.main;

import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.TrainingDto;
import br.ei9.mmgrupo.academy.model.realm.UserDto;
import br.ei9.mmgrupo.academy.model.service.ProfileResponse;
import br.ei9.mmgrupo.academy.rest.RestClient;
import br.ei9.mmgrupo.academy.view.Utils;
import br.ei9.mmgrupo.academy.view.dialog.MyDialogFragment;

/**
 * Created by vini on 11/01/18.
 */

@OptionsMenu(R.menu.card)
@EFragment(R.layout.fragment_card)
public class FragmentCard extends MyFragment implements View.OnClickListener, ExpandableListView.OnChildClickListener{

    @RestService
    RestClient rest;

    @ViewById(R.id.training_list)
    ExpandableListView expListView;

    ExpandableListAdapter listAdapter;
    List<TrainingHeaderItem> listDataHeader;
    HashMap<TrainingHeaderItem, List<TrainingChildItem>> listDataChild;

    AlertDialog progressDialog;

    @AfterViews
    public void onCreate() {
        if (isReadyForRequest()) {
            progressDialog = createProgressDialog();
            updateViewFromServer();
        }else{
            updateView();
        }

        expListView.setOnChildClickListener(this);


    }

    @Background
    public void updateViewFromServer() {
        rest.setHeader("Content-Type", "application/json");
        if (Utils.acess_token.equals("")) {
            List<UserDto> user = (List<UserDto>) RealmObjectManipulate.getAll(UserDto.class);
            rest.setHeader("uid", user.get(0).getEmail());
            rest.setHeader("client", user.get(0).getClient());
            rest.setHeader("access-token", user.get(0).getToken());
        } else {
            rest.setHeader("uid", Utils.uid);
            rest.setHeader("client", Utils.client);
            rest.setHeader("access-token", Utils.acess_token);
        }

        rest.getCardResponse();
        verifyRequest(Utils.responseList);
    }

    @UiThread
    public void updateView(){

        prepareListData();
        listAdapter = new TrainingExpandableListAdapter(getContext(), listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<TrainingHeaderItem>();
        listDataChild = new HashMap<TrainingHeaderItem, List<TrainingChildItem>>();

        List<TrainingDto> trainings = getListOfTraining();

        if(trainings.size() > 0){

            for(int i=0;i<trainings.size();i++){
                TrainingHeaderItem item = new TrainingHeaderItem(trainings.get(i).getGroup(),trainings.get(i).getTurn());
                TrainingChildItem childItem = new TrainingChildItem(trainings.get(i).getEquipament(),trainings.get(i).getExercise(),trainings.get(i).getNumber(),trainings.get(i).getRepetition(),trainings.get(i).getMuscleType(),trainings.get(i).getObs());
                if(listDataHeader.size() == 0){
                    listDataHeader.add(item);
                    listDataHeader.get(0).add(childItem);
                }else{
                    int count = listDataHeader.size();
                    for(int x=0;x<listDataHeader.size();x++){
                        if(listDataHeader.get(x).getGroup().equals(item.getGroup()) && listDataHeader.get(x).getTurn().equals(item.getTurn())){
                            listDataHeader.get(x).add(childItem);
                            break;
                        }else{
                            count --;
                        }
                    }

                    if(count == 0){
                        listDataHeader.add(item);
                        listDataHeader.get(listDataHeader.size()-1).add(childItem);
                    }
                }

            }


            for(int i=0;i<listDataHeader.size();i++){
                listDataChild.put(listDataHeader.get(i),listDataHeader.get(i).getItens());
            }

        }

    }

    protected void verifyRequest(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getInt("status") == 200){
                saveCardOnDataBase(Utils.responseList);
                updateView();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        finishProgressDialog(progressDialog);
    }

    protected void saveCardOnDataBase(String response){
        String json = response.replace("\"status\":200,","");
        JSONObject jsonObject;
        JSONArray training;

        try {
            jsonObject = new JSONObject(json);
            training = jsonObject.getJSONArray("message");
            for(int i=0;i<training.length();i++){
                Integer id = Integer.parseInt(training.getJSONObject(i).get("id").toString());
                String coach = training.getJSONObject(i).get("coach").toString();
                    String group = training.getJSONObject(i).get("group").toString();
                String turn = training.getJSONObject(i).get("turn").toString();
                Date date = convertToDate(training.getJSONObject(i).get("date").toString());
                String equipament = training.getJSONObject(i).get("equipament").toString();
                String exercise = training.getJSONObject(i).get("exercise").toString();
                String number = training.getJSONObject(i).get("number").toString();
                String repetition = training.getJSONObject(i).get("repetition").toString();
                String muscleType = training.getJSONObject(i).get("muscle_type").toString();
                String obs = training.getJSONObject(i).get("obs").toString();
                RealmObjectManipulate.save(new TrainingDto(id,coach,group,turn,date,equipament,exercise,number,repetition,muscleType,obs));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public List<TrainingDto> getListOfTraining(){
        return (List<TrainingDto>) RealmObjectManipulate.getAll(TrainingDto.class);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

        TrainingChildItem item =  (TrainingChildItem) listAdapter.getChild(groupPosition,childPosition);

        if(!item.getObs().equals("")){
            // DialogFragment.show() will take care of adding the fragment
            // in a transaction.  We also want to remove any currently showing
            // dialog, so make our own transaction and take care of that here.
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            // Create and show the dialog.

            MyDialogFragment newFragment = MyDialogFragment.newInstance(1);
            newFragment.setContent(item.getObs());
            newFragment.show(ft, "dialog");
        }

        return false;
    }

    @OptionsItem(R.id.option_info)
    public void clickOnOptionInfo() {

        List<TrainingDto> trainings = getListOfTraining();
        TrainingDto itemDto = trainings.get(0);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        MyDialogFragment newFragment = MyDialogFragment.newInstance(5);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy, hh:mm:ss");
        String today = formatter.format(itemDto.getDate());
        newFragment.setTitle(getResources().getString(R.string.information));
        newFragment.setContent("Essa ficha foi feita pelo profissional: " + itemDto.getCoach() +
                ". No dia: " + today);
        newFragment.show(ft, "dialog");


    }

}
