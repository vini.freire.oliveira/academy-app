package br.ei9.mmgrupo.academy.model.service;

import java.io.Serializable;

/**
 * Created by vini on 23/12/17.
 */

public class Login implements Serializable {
    private String email;
    private String password;

    public Login(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
