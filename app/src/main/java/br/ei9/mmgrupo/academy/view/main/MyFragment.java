package br.ei9.mmgrupo.academy.view.main;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.ei9.mmgrupo.academy.R;
import dmax.dialog.SpotsDialog;

/**
 * Created by vini on 24/01/18.
 */

public class MyFragment extends Fragment {

    public boolean isReadyForRequest(){
        boolean isOn = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                isOn = true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                isOn = true;
            }
        } else {
            isOn = false;
        }
        return isOn;
    }

    public AlertDialog createProgressDialog(){

        AlertDialog progressDialog = new SpotsDialog(getContext(), R.style.Custom);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;

    }

    public void finishProgressDialog(AlertDialog progressDialog){
        progressDialog.dismiss();
    }

    public Date convertToDate(String dateString){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format.parse(dateString);
            return  date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

}
