package br.ei9.mmgrupo.academy.view.login;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.ei9.mmgrupo.academy.model.realm.GymDto;
import br.ei9.mmgrupo.academy.model.realm.PlanDto;
import br.ei9.mmgrupo.academy.model.realm.ProfileDto;
import br.ei9.mmgrupo.academy.model.service.MobileProfileResponse;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmAccess;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.UserDto;
import br.ei9.mmgrupo.academy.model.service.Login;
import br.ei9.mmgrupo.academy.model.service.LoginResponse;
import br.ei9.mmgrupo.academy.model.service.ProfileResponse;
import br.ei9.mmgrupo.academy.model.service.ValidateToken;
import br.ei9.mmgrupo.academy.rest.RestClient;
import br.ei9.mmgrupo.academy.view.Utils;
import br.ei9.mmgrupo.academy.view.main.MainActivity_;
import br.ei9.mmgrupo.academy.view.main.MyActivity;

@EActivity(R.layout.activity_login)
public class LoginActivity extends MyActivity {

    @RestService
    RestClient rest;

    @ViewById(R.id.user_email)
    EditText email;

    @ViewById(R.id.user_password)
    EditText password;

    @ViewById(R.id.user_reset_password)
    TextView resetPassword;

    AlertDialog progressDialog;

    @AfterViews
    public void initActivity(){
        resetPassword.setPaintFlags(resetPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        RealmAccess.configRealm(this);
        List<UserDto> users = (List<UserDto>) RealmObjectManipulate.getAll(UserDto.class);
        if(users.size() == 1){
            Utils.acess_token = users.get(0).getToken();
            Utils.client = users.get(0).getClient();
            Utils.uid = users.get(0).getEmail();
            if(isReadyForRequest()){
                progressDialog = createProgressDialog();
                validateToken();
            }else{
                startActivity(new Intent(this,MainActivity_.class));
                finish();
            }

        }
    }

    @Click(R.id.user_login)
    public void clickOnLogin(){
        if(isReadyForRequest()){
            progressDialog = createProgressDialog();
            login();
        }else{
            Snackbar.make(findViewById(R.id.academy_login_activity),
                    R.string.error_internet, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Background
    public void login(){
        String userEmail = email.getText().toString();
        String userPassword = password.getText().toString();
        rest.setHeader("Content-Type", "application/json" );
        try{
            rest.login(new Login(userEmail,userPassword));
            verifyRequest(Utils.responseList);
        }catch (HttpClientErrorException e){
            Snackbar.make(findViewById(R.id.academy_login_activity),"Email ou senha inválidos",Toast.LENGTH_SHORT).show();
        }
    }

    @Background
    public void validateToken(){
        rest.setHeader("Content-Type", "application/json" );
        rest.setHeader("uid",Utils.uid);
        rest.setHeader("access-token",Utils.acess_token);
        rest.setHeader("client",Utils.client);
        String response = "";
        try{
            rest.validateToken();
            response = Utils.responseList;
            //verifyToken(token);
        }catch (Exception e){
            response = e.getMessage();
            Log.d("#",e.getMessage());
        }

        verifyToken(response);
    }

    @Background
    public void getMobileProfile(){
        rest.setHeader("Content-Type", "application/json" );
        rest.setHeader("uid",Utils.uid);
        rest.setHeader("access-token",Utils.acess_token);
        rest.setHeader("client",Utils.client);
        rest.getMobileProfile();
        verifyRequestProfile(Utils.responseList);
    }

    @Background
    public void downloadCard(){
        rest.setHeader("Content-Type", "application/json" );
        rest.setHeader("uid",Utils.uid);
        rest.setHeader("access-token",Utils.acess_token);
        rest.setHeader("client",Utils.client);
        rest.getCardResponse();
        verifyProfileResponse(Utils.responseList);
    }

    @Background
    public void downloadMeasures(){
        rest.setHeader("Content-Type", "application/json" );
        rest.setHeader("uid",Utils.uid);
        rest.setHeader("access-token",Utils.acess_token);
        rest.setHeader("client",Utils.client);
        rest.getResponse();
        verifyMeasuresRequest(Utils.responseList);
    }

    public void verifyToken(String token){
        finishProgressDialog(progressDialog);
        if(!token.equals("401 Unauthorized")){
            startActivity(new Intent(this,MainActivity_.class));
            finish();
        }
    }

    public void verifyProfileResponse(String response){

        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getInt("status") == 200){
                saveCardOnDataBase(Utils.responseList);
                downloadMeasures();
            }else{
                finishProgressDialog(progressDialog);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void verifyRequest(String login){

        if(login != null){
            try {
                JSONObject jsonObj = new JSONObject(login);
                if(jsonObj.getJSONObject("data") != null){
                    UserDto userDto = new UserDto(1, jsonObj.getJSONObject("data").getString("uid"), Utils.client, Utils.acess_token);
                    saveOnDataBase(userDto);
                    getMobileProfile();
                }else{
                    finishProgressDialog(progressDialog);
                    Snackbar.make(findViewById(R.id.academy_login_activity),"Não autorizado",Toast.LENGTH_SHORT).show();
                }

            }catch (JSONException e){
                e.printStackTrace();
            }

        }

    }

    public void verifyMeasuresRequest(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getInt("status") == 200){
                saveMeasuresOnDatabase(Utils.responseList);
                finishProgressDialog(progressDialog);
                startActivity(new Intent(this,MainActivity_.class));
                finish();
            }else{
                finishProgressDialog(progressDialog);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void verifyRequestProfile(String data){

        if(data != null){
            try{
                JSONObject jsonObj = new JSONObject(data);
                if(jsonObj.getJSONObject("message") != null){
                    if(jsonObj.getJSONObject("message").getJSONObject("profile") != null){
                        JSONObject profile = jsonObj.getJSONObject("message").getJSONObject("profile");
                        saveOnDataBase(new ProfileDto(profile.getInt("id"),profile.getString("address"),profile.getString("phone"),profile.getString("name"),profile.getString("cpf"),convertToDate(profile.getString("created_at")),convertToDate(profile.getString("updated_at"))));
                    }
                    if(jsonObj.getJSONObject("message").getJSONObject("gym") != null){
                        JSONObject gym = jsonObj.getJSONObject("message").getJSONObject("gym");
                        saveOnDataBase(new GymDto(gym.getInt("id"),gym.getString("cnpj"),gym.getString("name"),gym.getString("address"),gym.getString("cep"),gym.getString("city"),gym.getString("state"),gym.getString("phone"),gym.getString("cellphone"),gym.getString("lat"),gym.getString("lon"),convertToDate(gym.getString("created_at")),convertToDate(gym.getString("updated_at"))));
                    }
                    if(jsonObj.getJSONObject("message").getJSONObject("plan_client") != null){
                        JSONObject plan = jsonObj.getJSONObject("message").getJSONObject("plan_client");
                        saveOnDataBase(new PlanDto(plan.getInt("id"),plan.getString("name"),Float.parseFloat(plan.getString("price")),convertToDate(plan.getString("created_at")),convertToDate(plan.getString("updated_at"))));
                    }
                    downloadCard();
                }else{
                    finishProgressDialog(progressDialog);
                }

            }catch (JSONException e){
                e.printStackTrace();
            }
        }

    }

}
