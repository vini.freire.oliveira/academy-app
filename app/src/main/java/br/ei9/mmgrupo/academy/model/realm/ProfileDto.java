package br.ei9.mmgrupo.academy.model.realm;

import java.util.Date;

import br.ei9.mmgrupo.academy.model.service.MobileProfileResponse;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by vini on 13/01/18.
 */
@RealmClass
public class ProfileDto extends RealmObject {

    @PrimaryKey
    protected Integer id;

    protected String address;
    protected String phone;
    protected String name;
    protected String cpf;
    protected Date createdAt;
    protected Date updatedAt;

    public ProfileDto() {
    }

    public ProfileDto(Integer id, String address, String phone, String name, String cpf, Date createdAt, Date updatedAt) {
        this.id = id;
        this.address = address;
        this.phone = phone;
        this.name = name;
        this.cpf = cpf;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public ProfileDto(MobileProfileResponse.ProfileContent.ProfileMobile profile) {
        this.id = profile.getId();
        this.address = profile.getAddress();
        this.phone = profile.getPhone();
        this.name = profile.getName();
        this.cpf = profile.getCpf();
        this.createdAt = profile.getCreated_at();
        this.updatedAt = profile.getUpdated_at();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}

