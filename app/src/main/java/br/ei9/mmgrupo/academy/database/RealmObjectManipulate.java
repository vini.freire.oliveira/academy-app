package br.ei9.mmgrupo.academy.database;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by vini on 17/12/17.
 */

public class RealmObjectManipulate {


    public static void deleteAll(final Class<? extends RealmObject> realmClass){

        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        RealmResults<? extends RealmObject> results = realm.where( realmClass ).findAll();
        results.clear();
        realm.commitTransaction();

    }

    public static List<? extends RealmObject> getAll(final Class<? extends RealmObject> realmClass){

        Realm realm = Realm.getDefaultInstance();

        final List<RealmObject> list = new ArrayList<>();

        realm.executeTransaction(new Realm.Transaction() {
            public void execute(Realm realm) {
                RealmResults<? extends RealmObject > results = realm.where(realmClass).findAll();
                list.addAll(results);
            }
        });
        return list;
    }

    public static void save(final RealmObject item ){

        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(item);
            }
        });
    }


}