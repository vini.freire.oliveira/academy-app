package br.ei9.mmgrupo.academy.model.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by vini on 24/01/18.
 */

@RealmClass
public class PaymentDto extends RealmObject {

    @PrimaryKey
    protected Integer id;

    protected Date date;
    protected Float value;

    public PaymentDto() {
    }

    public PaymentDto(Integer id, Date date, Float value) {
        this.id = id;
        this.date = date;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }


}
