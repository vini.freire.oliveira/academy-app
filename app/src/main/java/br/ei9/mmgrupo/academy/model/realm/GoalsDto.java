package br.ei9.mmgrupo.academy.model.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by vini on 28/02/18.
 */

@RealmClass
public class GoalsDto extends RealmObject {

    @PrimaryKey
    protected Integer id;

    protected Float weight;
    protected Float weightGoal;

    protected Date date;
    protected Date dateGoal;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getWeightGoal() {
        return weightGoal;
    }

    public void setWeightGoal(Float weightGoal) {
        this.weightGoal = weightGoal;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDateGoal() {
        return dateGoal;
    }

    public void setDateGoal(Date dateGoal) {
        this.dateGoal = dateGoal;
    }
}
