package br.ei9.mmgrupo.academy.model.service;

import java.io.Serializable;

/**
 * Created by vini on 17/03/18.
 */

public class ValidateToken implements Serializable {
    private boolean sucess;

    public ValidateToken() {
    }

    public ValidateToken(boolean sucess) {
        this.sucess = sucess;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }
}
