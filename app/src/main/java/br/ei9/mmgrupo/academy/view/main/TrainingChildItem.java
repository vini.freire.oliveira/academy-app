package br.ei9.mmgrupo.academy.view.main;

import java.io.Serializable;

/**
 * Created by vini on 26/01/18.
 */

public class TrainingChildItem implements Serializable {

    protected String equipament;
    protected String exercise;
    protected String number;
    protected String repetition;
    protected String muscleType;
    protected String obs;

    public TrainingChildItem() {
    }

    public TrainingChildItem(String equipament, String exercise, String number, String repetition, String muscleType, String obs) {
        this.equipament = equipament;
        this.exercise = exercise;
        this.number = number;
        this.repetition = repetition;
        this.muscleType = muscleType;
        this.obs = obs;
    }

    public String getEquipament() {
        return equipament;
    }

    public void setEquipament(String equipament) {
        this.equipament = equipament;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRepetition() {
        return repetition;
    }

    public void setRepetition(String repetition) {
        this.repetition = repetition;
    }

    public String getMuscleType() {
        return muscleType;
    }

    public void setMuscleType(String muscleType) {
        this.muscleType = muscleType;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }
}
