package br.ei9.mmgrupo.academy.view.main;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vini on 24/01/18.
 */

public class PaymentsItem implements Serializable{

    protected Integer id;
    protected Date date;
    protected Float value;

    public PaymentsItem(Date date, Float value) {
        this.date = date;
        this.value = value;
    }

    public PaymentsItem() {
    }

    public PaymentsItem(Integer id, Date date, Float value) {
        this.id = id;
        this.date = date;
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
