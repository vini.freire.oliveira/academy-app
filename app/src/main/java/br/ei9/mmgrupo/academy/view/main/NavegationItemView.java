package br.ei9.mmgrupo.academy.view.main;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.ei9.mmgrupo.academy.R;

/**
 * Created by vini on 11/01/18.
 */

@EViewGroup(R.layout.item_view_navegation)
public class NavegationItemView extends LinearLayout {

    @ViewById(R.id.navegation_item_image)
    ImageView image;

    @ViewById(R.id.navegation_item_text)
    TextView text;

    public NavegationItemView(Context context) {
        super(context);
    }

    public void bind(NavegationItem item){
        if(item.getImage() != 0){
            this.image.setBackgroundResource(item.getImage());
        }
        this.text.setText(item.getItem());
    }

}
