package br.ei9.mmgrupo.academy.view.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.DailyTrainingDto;
import br.ei9.mmgrupo.academy.model.realm.TrainingDto;
import br.ei9.mmgrupo.academy.view.interfaces.DialogInterface;
import br.ei9.mmgrupo.academy.view.main.TrainingChildItem;
import br.ei9.mmgrupo.academy.view.main.TrainingHeaderItem;

/**
 * Created by vini on 30/01/18.
 */

public class TrainingDialogFragment extends DialogFragment{

    private DialogInterface dialogInterface;
    private TrainingDialogFragment trainingDialogFragment;
    private DailyTrainingDto dailyTrainingDto;
    private TextView timeView;
    private TextView idTrainingView;
    private TextView nunTrainingView;
    private int currentTraining;
    private int num;
    private int totalNum;
    private TextView muscleView;
    private TextView equipamentView;
    private TextView exerciseView;
    private TextView numberSerieView;
    private TextView obsView;
    private RadioButton isExerciseDone;
    private List<Boolean> exerciseDone;
    private long initialTime;
    private Handler handler;
    private boolean isRunning;
    private final long MILLIS_IN_SEC = 1000L;
    private final int SECS_IN_MIN = 60;
    private List<TrainingHeaderItem> listDataHeader;
    private HashMap<TrainingHeaderItem, List<TrainingChildItem>> listDataChild;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dialog_fragment_training, container, false);
        getDialog().setTitle(getString(R.string.my_training));
        startClock();
        prepareListData();
        this.currentTraining = getCurrentTraining();
        this.num = 0;
        if(listDataChild.size()> 0){
            this.totalNum = listDataChild.get(listDataHeader.get(currentTraining)).size();
        }else{
            this.totalNum = 0;
            getDialog().dismiss();
        }
        createDatabaseTraining();
        setExerciseDoneToFalse();
        idTrainingView = (TextView) v.findViewById(R.id.id_training);
        nunTrainingView = (TextView) v.findViewById(R.id.daily_training_num);
        muscleView = (TextView) v.findViewById(R.id.muscle);
        equipamentView = (TextView) v.findViewById(R.id.equipament);
        exerciseView = (TextView) v.findViewById(R.id.exercise);
        numberSerieView = (TextView) v.findViewById(R.id.number_serie);
        obsView = (TextView) v.findViewById(R.id.obs);
        timeView = (TextView) v.findViewById(R.id.clock);
        Button endTraining = (Button) v.findViewById(R.id.end_training);
        Button nextExercise = (Button) v.findViewById(R.id.button_next);
        Button previousExercise = (Button) v.findViewById(R.id.button_previous);
        isExerciseDone = (RadioButton) v.findViewById(R.id.end_exercise);

        setViewForExercise();

        endTraining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exerciseDone.set(num,isExerciseDone.isChecked()) ;
                dailyTrainingDto.setEndedAt(new Date());
                dailyTrainingDto.setExerciseDone(getValueOfExerciseDone());
                TrainingConfirmationDialogFragment newFragment = new TrainingConfirmationDialogFragment();
                newFragment.setContent(trainingDialogFragment,dialogInterface, dailyTrainingDto, newFragment, "Você tem certeza que deseja finalizar o treino ?");
                newFragment.show(getFragmentManager(), "dialog");
            }
        });

        nextExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exerciseDone.set(num,isExerciseDone.isChecked()) ;
                num++;
                setViewForExercise();
            }
        });

        previousExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exerciseDone.set(num,isExerciseDone.isChecked()) ;
                num++;
                setViewForExercise();
            }
        });

        return v;
    }

    protected void startClock(){
        handler = new Handler();
        isRunning = true;
        initialTime = System.currentTimeMillis();
        handler.postDelayed(runnable, MILLIS_IN_SEC);

    }

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (isRunning) {
                long seconds = (System.currentTimeMillis() - initialTime) / MILLIS_IN_SEC;
                timeView.setText(String.format("%02d:%02d", seconds / SECS_IN_MIN, seconds % SECS_IN_MIN));
                handler.postDelayed(runnable, MILLIS_IN_SEC);
            }
        }
    };

    protected void setViewForExercise(){

        num = num % (totalNum);

        idTrainingView.setText(listDataHeader.get(currentTraining).getGroup());
        nunTrainingView.setText((num+1)+" de "+totalNum);
        muscleView.setText(listDataChild.get(listDataHeader.get(currentTraining)).get(num).getMuscleType());
        equipamentView.setText(listDataChild.get(listDataHeader.get(currentTraining)).get(num).getEquipament());
        exerciseView.setText(listDataChild.get(listDataHeader.get(currentTraining)).get(num).getExercise());
        numberSerieView.setText(listDataChild.get(listDataHeader.get(currentTraining)).get(num).getNumber()
        +"x"+listDataChild.get(listDataHeader.get(currentTraining)).get(num).getRepetition());
        obsView.setText(listDataChild.get(listDataHeader.get(currentTraining)).get(num).getObs());
        isExerciseDone.setChecked(exerciseDone.get(num));

    }

    public void closeDialog(){
        getDialog().dismiss();
    }

    private int getCurrentTraining(){
        List<DailyTrainingDto> trainingDtos = (List<DailyTrainingDto>) RealmObjectManipulate.getAll(DailyTrainingDto.class);
        if(trainingDtos.size() > 0){
            return (trainingDtos.get(trainingDtos.size() - 1).getTrainingID() + 1)  % listDataHeader.size();
        }else{
            return 0;
        }
    }

    private void prepareListData() {

        listDataHeader = new ArrayList<TrainingHeaderItem>();
        listDataChild = new HashMap<TrainingHeaderItem, List<TrainingChildItem>>();

        List<TrainingDto> trainings = getListOfTraining();

        if(trainings.size() > 0){

            for(int i=0;i<trainings.size();i++){
                TrainingHeaderItem item = new TrainingHeaderItem(trainings.get(i).getGroup(),trainings.get(i).getTurn());
                TrainingChildItem childItem = new TrainingChildItem(trainings.get(i).getEquipament(),trainings.get(i).getExercise(),trainings.get(i).getNumber(),trainings.get(i).getRepetition(),trainings.get(i).getMuscleType(),trainings.get(i).getObs());
                if(listDataHeader.size() == 0){
                    listDataHeader.add(item);
                    listDataHeader.get(0).add(childItem);
                }else{
                    int count = listDataHeader.size();
                    for(int x=0;x<listDataHeader.size();x++){
                        if(listDataHeader.get(x).getGroup().equals(item.getGroup()) && listDataHeader.get(x).getTurn().equals(item.getTurn())){
                            listDataHeader.get(x).add(childItem);
                            break;
                        }else{
                            count --;
                        }
                    }

                    if(count == 0){
                        listDataHeader.add(item);
                        listDataHeader.get(listDataHeader.size()-1).add(childItem);
                    }
                }

            }


            for(int i=0;i<listDataHeader.size();i++){
                listDataChild.put(listDataHeader.get(i),listDataHeader.get(i).getItens());
            }

        }

    }

    public List<TrainingDto> getListOfTraining(){
        return (List<TrainingDto>) RealmObjectManipulate.getAll(TrainingDto.class);
    }

    protected void createDatabaseTraining(){
        int index = RealmObjectManipulate.getAll(DailyTrainingDto.class).size();
        dailyTrainingDto = new DailyTrainingDto(index,currentTraining,new Date());
    }

    protected void setExerciseDoneToFalse(){
        exerciseDone = new ArrayList<>();
        List<TrainingChildItem> itens = listDataChild.get(listDataHeader.get(currentTraining));

        for(int i=0; i<itens.size(); i++){
            exerciseDone.add(false);
        }

    }

    protected float getValueOfExerciseDone(){
        int allTrue = 0;
        for(int i=0;i<exerciseDone.size();i++){
            if(exerciseDone.get(i)){
                allTrue++;
            }
        }

        return (100*allTrue)/exerciseDone.size();
    }

    public void setDialogInterface(TrainingDialogFragment trainingDialogFragment,DialogInterface dialogInterface) {
        this.dialogInterface = dialogInterface;
        this.trainingDialogFragment = trainingDialogFragment;
    }
}
