package br.ei9.mmgrupo.academy.view.main;

import android.app.AlertDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.MeasuresDto;
import br.ei9.mmgrupo.academy.model.realm.UserDto;
import br.ei9.mmgrupo.academy.model.service.ProfileResponse;
import br.ei9.mmgrupo.academy.rest.RestClient;
import br.ei9.mmgrupo.academy.view.Utils;
import br.ei9.mmgrupo.academy.view.dialog.MeasuresDialogFragment;
import br.ei9.mmgrupo.academy.view.dialog.MyDialogFragment;

/**
 * Created by vini on 11/01/18.
 */

@OptionsMenu(R.menu.card)
@EFragment(R.layout.fragment_medidas)
public class FragmentMeasures extends MyFragment {

    @RestService
    RestClient rest;

    @ViewById(R.id.height)
    TextView height;

    @ViewById(R.id.weight)
    TextView weight;

    @ViewById(R.id.waist)
    TextView waist;

    @ViewById(R.id.pectoral)
    TextView pectoral;

    @ViewById(R.id.hip)
    TextView hip;

    @ViewById(R.id.arm_right)
    TextView armRight;

    @ViewById(R.id.arm_left)
    TextView armLeft;

    @ViewById(R.id.thigh_right)
    TextView thighRight;

    @ViewById(R.id.thigh_left)
    TextView thighLeft;

    @ViewById(R.id.calf_right)
    TextView calfRight;

    @ViewById(R.id.calf_left)
    TextView calfLeft;

    AlertDialog progressDialog;

    @AfterViews
    public void onCreate() {
        if (isReadyForRequest()) {
            progressDialog = createProgressDialog();
            updateView();
        } else {
            setTextViewValues();
        }
    }

    @Background
    public void updateView() {
        rest.setHeader("Content-Type", "application/json");
        if (Utils.acess_token.equals("")) {
            List<UserDto> user = (List<UserDto>) RealmObjectManipulate.getAll(UserDto.class);
            rest.setHeader("uid", user.get(0).getEmail());
            rest.setHeader("client", user.get(0).getClient());
            rest.setHeader("access-token", user.get(0).getToken());
        } else {
            rest.setHeader("uid", Utils.uid);
            rest.setHeader("client", Utils.client);
            rest.setHeader("access-token", Utils.acess_token);
        }
        rest.getResponse();
        verifyRequest(Utils.responseList);
    }

    protected void verifyRequest(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getInt("status") == 200){
                saveMeasuresOnDatabase(Utils.responseList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setTextViewValues();
        finishProgressDialog(progressDialog);
    }

    protected List<MeasuresDto> getValuesFromBd(){
        return (List<MeasuresDto>)RealmObjectManipulate.getAll(MeasuresDto.class);
    }

    @UiThread
    protected void setTextViewValues(){
        List<MeasuresDto> measures = getValuesFromBd();

        if(measures.size()>0){
            String height = Float.toString(measures.get(measures.size() - 1).getHeight());
            this.height.setText(height.substring(0,1) + "." + height.substring(1,3) + " m");
            this.weight.setText(Float.toString(measures.get(measures.size() - 1).getWeight())+" Kg");
            this.waist.setText(Float.toString(measures.get(measures.size() - 1).getWaist()));
            this.pectoral.setText(Float.toString(measures.get(measures.size() - 1).getPectoral()));
            this.hip.setText(Float.toString(measures.get(measures.size() - 1).getHip()));
            this.armRight.setText(Float.toString(measures.get(measures.size() - 1).getArmRight()));
            this.armLeft.setText(Float.toString(measures.get(measures.size() - 1).getArmLeft()));
            this.thighRight.setText(Float.toString(measures.get(measures.size() - 1).getThighRight()));
            this.thighLeft.setText(Float.toString(measures.get(measures.size() - 1).getThighLeft()));
            this.calfRight.setText(Float.toString(measures.get(measures.size() - 1).getCalfRight()));
            this.calfLeft.setText(Float.toString(measures.get(measures.size() - 1).getCalfLeft()));
        }else{
            Snackbar.make(getView(),"Entre em contato com o responsável para o cadastro de medidas", Snackbar.LENGTH_SHORT).show();
        }
    }

    protected void saveMeasuresOnDatabase(String response){
        String json = response.replace("\"status\":200,","");
        JSONObject jsonObject;
        JSONArray measures;

        try {
            jsonObject = new JSONObject(json);
            measures = jsonObject.getJSONArray("message");
            for(int i=0;i<measures.length();i++){
                Integer id = Integer.parseInt(measures.getJSONObject(i).get("id").toString());
                Float weight = Float.parseFloat(measures.getJSONObject(i).get("weight").toString());
                Float height = Float.parseFloat(measures.getJSONObject(i).get("height").toString());
                Float waist = Float.parseFloat(measures.getJSONObject(i).get("waist").toString());
                Float pectoral = Float.parseFloat(measures.getJSONObject(i).get("pectoral").toString());
                Float hip = Float.parseFloat(measures.getJSONObject(i).get("hip").toString());
                Float armRight = Float.parseFloat(measures.getJSONObject(i).get("arm_right").toString());
                Float armLeft = Float.parseFloat(measures.getJSONObject(i).get("arm_left").toString());
                Float thighRight = Float.parseFloat(measures.getJSONObject(i).get("thigh_right").toString());
                Float thighLeft = Float.parseFloat(measures.getJSONObject(i).get("thigh_left").toString());
                Float calfRight = Float.parseFloat(measures.getJSONObject(i).get("calf_right").toString());
                Float calfLeft = Float.parseFloat(measures.getJSONObject(i).get("calf_left").toString());
                Log.d("#",measures.getJSONObject(i).get("created_at").toString());
                Date date = convertToDate(measures.getJSONObject(i).get("created_at").toString());
                RealmObjectManipulate.save(new MeasuresDto(id,weight,height,waist,pectoral,hip,armRight,armLeft,
                        thighRight,thighLeft,calfRight,calfLeft,date));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OptionsItem(R.id.option_info)
    public void clickOnOptionInfo() {

        List<MeasuresDto> mesuares = getValuesFromBd();
        MeasuresDto measure = mesuares.get(mesuares.size() - 1);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        MeasuresDialogFragment newFragment = MeasuresDialogFragment.newInstance(8);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy, hh:mm:ss");
        newFragment.setTitle(getResources().getString(R.string.my_measures));

        String height = Float.toString(measure.getHeight());

        newFragment.setContent("Altura: " + height.substring(0,1) + "." + height.substring(1,3) + " m\n"+
                "Peso: " + measure.getWeight() + " kg\n"+
                "Peitoral: " + measure.getPectoral() + " cm\n" +
                "Cintura: " + measure.getWaist() + " cm\n" +
                "Quadril: " + measure.getHip() + " cm\n"+
                "Braço direito: " + measure.getArmRight() + " cm\n" +
                "Braço esquerdo: " + measure.getArmLeft() +" cm\n"+
                "Coxa direita: " + measure.getThighRight() + " cm\n" +
                "Coxa esquerda: " + measure.getThighLeft() + " cm\n"+
                "Panturilha direita: " + measure.getCalfRight() + " cm\n"+
                "Panturilha esquerda: " + measure.getCalfLeft() + "cm\n");

        newFragment.show(ft, "dialog");


    }

}