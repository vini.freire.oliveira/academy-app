package br.ei9.mmgrupo.academy.database;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by vini on 17/12/17.
 */

public class RealmAccess{

    public static  void configRealm(Context context){

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }
}