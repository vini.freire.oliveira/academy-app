package br.ei9.mmgrupo.academy.view.main;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.ei9.mmgrupo.academy.*;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.GymDto;
import br.ei9.mmgrupo.academy.model.realm.MeasuresDto;
import br.ei9.mmgrupo.academy.model.realm.PlanDto;
import br.ei9.mmgrupo.academy.model.realm.ProfileDto;
import br.ei9.mmgrupo.academy.model.realm.TrainingDto;
import br.ei9.mmgrupo.academy.model.realm.UserDto;
import dmax.dialog.SpotsDialog;

/**
 * Created by vini on 06/01/18.
 */

public class MyActivity extends AppCompatActivity {


    public boolean isReadyForRequest(){
        boolean isOn = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                isOn = true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                isOn = true;
            }
        } else {
            isOn = false;
        }
        return isOn;
    }

    public Date toDate(String dateString) {
        return new Date(dateString);
    }

    public AlertDialog createProgressDialog(){

        AlertDialog progressDialog = new SpotsDialog(this, R.style.Custom);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;

    }

    public void finishProgressDialog(AlertDialog progressDialog){
        progressDialog.dismiss();
    }

    public void saveOnDataBase(UserDto item){
        RealmObjectManipulate.save(item);
    }

    public void saveOnDataBase(ProfileDto item){
        RealmObjectManipulate.save(item);
    }

    public void saveOnDataBase(GymDto item){
        RealmObjectManipulate.save(item);
    }

    public void saveOnDataBase(PlanDto item){
        RealmObjectManipulate.save(item);
    }


    protected void saveCardOnDataBase(String response){
        String json = response.replace("\"status\":200,","");
        JSONObject jsonObject;
        JSONArray training;

        try {
            jsonObject = new JSONObject(json);
            training = jsonObject.getJSONArray("message");
            for(int i=0;i<training.length();i++){
                Integer id = Integer.parseInt(training.getJSONObject(i).get("id").toString());
                String coach = training.getJSONObject(i).get("coach").toString();
                String group = training.getJSONObject(i).get("group").toString();
                String turn = training.getJSONObject(i).get("turn").toString();
                Date date = convertToDate(training.getJSONObject(i).get("date").toString());
                String equipament = training.getJSONObject(i).get("equipament").toString();
                String exercise = training.getJSONObject(i).get("exercise").toString();
                String number = training.getJSONObject(i).get("number").toString();
                String repetition = training.getJSONObject(i).get("repetition").toString();
                String muscleType = training.getJSONObject(i).get("muscle_type").toString();
                String obs = training.getJSONObject(i).get("obs").toString();
                RealmObjectManipulate.save(new TrainingDto(id,coach,group,turn,date,equipament,exercise,number,repetition,muscleType,obs));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public Date convertToDate(String dateString){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format.parse(dateString);
            return  date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    protected void saveMeasuresOnDatabase(String response){
        String json = response.replace("\"status\":200,","");
        JSONObject jsonObject;
        JSONArray measures;

        try {
            jsonObject = new JSONObject(json);
            measures = jsonObject.getJSONArray("message");
            for(int i=0;i<measures.length();i++){
                Integer id = Integer.parseInt(measures.getJSONObject(i).get("id").toString());
                Float weight = Float.parseFloat(measures.getJSONObject(i).get("weight").toString());
                Float height = Float.parseFloat(measures.getJSONObject(i).get("height").toString());
                Float waist = Float.parseFloat(measures.getJSONObject(i).get("waist").toString());
                Float pectoral = Float.parseFloat(measures.getJSONObject(i).get("pectoral").toString());
                Float hip = Float.parseFloat(measures.getJSONObject(i).get("hip").toString());
                Float armRight = Float.parseFloat(measures.getJSONObject(i).get("arm_right").toString());
                Float armLeft = Float.parseFloat(measures.getJSONObject(i).get("arm_left").toString());
                Float thighRight = Float.parseFloat(measures.getJSONObject(i).get("thigh_right").toString());
                Float thighLeft = Float.parseFloat(measures.getJSONObject(i).get("thigh_left").toString());
                Float calfRight = Float.parseFloat(measures.getJSONObject(i).get("calf_right").toString());
                Float calfLeft = Float.parseFloat(measures.getJSONObject(i).get("calf_left").toString());
                Log.d("#",measures.getJSONObject(i).get("created_at").toString());
                Date date = convertToDate(measures.getJSONObject(i).get("created_at").toString());
                RealmObjectManipulate.save(new MeasuresDto(id,weight,height,waist,pectoral,hip,armRight,armLeft,
                        thighRight,thighLeft,calfRight,calfLeft,date));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
