package br.ei9.mmgrupo.academy.view.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.MeasuresDto;
import br.ei9.mmgrupo.academy.view.interfaces.DialogInterface;

/**
 * Created by vini on 28/02/18.
 */

public class MeasuresListDialogFragment extends DialogFragment {
    protected DialogInterface dialogInterface;
    protected MeasuresListDialogFragment oldFragment;
    protected ListView measuresList;

    public void setDialogInterface(DialogInterface dialogInterface, MeasuresListDialogFragment oldFragment) {
        this.dialogInterface = dialogInterface;
        this.oldFragment = oldFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dialog_fragment_list_measures, container, false);
        measuresList = (ListView) v.findViewById(R.id.measures_dialog_list);

        final List<MeasuresDto> measuresDtos = (List<MeasuresDto>) RealmObjectManipulate.getAll(MeasuresDto.class);
        String [] dates = new String[measuresDtos.size()];

        for(int i=0; i<measuresDtos.size();i++){
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String measureDate = formatter.format(measuresDtos.get(i).getDate());
            dates[i] = measureDate;
        }

        ArrayAdapter<String> adapter
                = new ArrayAdapter<String>(getContext(), R.layout.my_text_view, dates);

        measuresList.setAdapter(adapter);

        measuresList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogInterface.clickOnConfirmation(oldFragment,measuresDtos.get(position));
            }
        });

        Button buttonCancel = (Button)v.findViewById(R.id.dialog_button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return v;
    }
}
