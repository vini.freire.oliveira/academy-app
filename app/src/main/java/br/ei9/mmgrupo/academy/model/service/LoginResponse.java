package br.ei9.mmgrupo.academy.model.service;

import java.io.Serializable;

/**
 * Created by vini on 23/12/17.
 */

public class LoginResponse implements Serializable {

    private LoginResponseContent data;

    public LoginResponse() {
    }

    public LoginResponse(LoginResponseContent data) {
        this.data = data;
    }

    public LoginResponseContent getData() {
        return data;
    }

    public void setData(LoginResponseContent data) {
        this.data = data;
    }

    public class LoginResponseContent implements Serializable{

        private Long id;
        private String email;
        private String provider;
        private String uid;
        private String name;
        private String nickname;
        private String image;


        public LoginResponseContent(){

        }

        public LoginResponseContent(Long id, String email, String provider, String uid, String name, String nickname, String image) {
            this.id = id;
            this.email = email;
            this.provider = provider;
            this.uid = uid;
            this.name = name;
            this.nickname = nickname;
            this.image = image;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }

}
