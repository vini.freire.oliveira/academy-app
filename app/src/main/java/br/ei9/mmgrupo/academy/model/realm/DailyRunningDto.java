package br.ei9.mmgrupo.academy.model.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by vini on 01/02/18.
 */

@RealmClass
public class DailyRunningDto extends RealmObject{

    @PrimaryKey
    protected Integer id;

    protected Float km;
    protected Float calories;
    protected Float time;
    protected Date day;

    public DailyRunningDto() {
    }

    public DailyRunningDto(Integer id, Float km, Float calories, Float time, Date day) {
        this.id = id;
        this.km = km;
        this.calories = calories;
        this.time = time;
        this.day = day;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getKm() {
        return km;
    }

    public void setKm(Float km) {
        this.km = km;
    }

    public Float getCalories() {
        return calories;
    }

    public void setCalories(Float calories) {
        this.calories = calories;
    }

    public Float getTime() {
        return time;
    }

    public void setTime(Float time) {
        this.time = time;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }
}
