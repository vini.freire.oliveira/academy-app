package br.ei9.mmgrupo.academy.view.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import br.ei9.mmgrupo.academy.R;

/**
 * Created by vini on 30/01/18.
 */

public class MeasuresDialogFragment extends DialogFragment {

    int mNum;
    protected String item;
    protected String title;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static MeasuresDialogFragment newInstance(int num) {
        MeasuresDialogFragment dialogFragment = new MeasuresDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        dialogFragment.setArguments(args);

        return dialogFragment;
    }

    public void setContent(String item){
        this.item = item;
    }

    public void setTitle(String title){
        this.title = title;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments().getInt("num");

        // Pick a style based on the num.
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        switch (mNum) {
            case 1: style = DialogFragment.STYLE_NO_TITLE; break;
            case 2: style = DialogFragment.STYLE_NO_FRAME; break;
            case 3: style = DialogFragment.STYLE_NO_INPUT; break;
            case 4: style = DialogFragment.STYLE_NORMAL; break;
            case 5: style = DialogFragment.STYLE_NORMAL; break;
            case 6: style = DialogFragment.STYLE_NO_TITLE; break;
            case 7: style = DialogFragment.STYLE_NO_FRAME; break;
            case 8: style = DialogFragment.STYLE_NORMAL; break;
        }
        switch (mNum) {
            case 5: theme = R.style.MyCustomDialogInfo; break; // tela vazia com a logo da academia
            case 6: theme = android.R.style.Theme_Holo_Light; break;
            case 7: theme = android.R.style.Theme_Holo_Light_Panel; break;
            case 8: theme = android.R.style.Theme_Holo; break; //tela cheia com a logo da academia
        }
        setStyle(style, theme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dialog_fragment_measures, container, false);
        TextView tv = (TextView) v.findViewById(R.id.dialog_text);

        tv.setText(this.item);

        if(this.title != null){
            getDialog().setTitle(this.title);
            // Set title divider color
            final int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
            View titleDivider = getDialog().findViewById(titleDividerId);
            if (titleDivider != null){
                titleDivider.setBackgroundColor(getResources().getColor(R.color.text_color));

            }
        }

        // Watch for button clicks.
        Button button = (Button)v.findViewById(R.id.dialog_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return v;
    }

    public void closeDialog(){
        getDialog().dismiss();
    }


}
