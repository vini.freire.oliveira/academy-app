package br.ei9.mmgrupo.academy.view.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.model.realm.DailyTrainingDto;
import br.ei9.mmgrupo.academy.view.interfaces.DialogInterface;

/**
 * Created by vini on 30/01/18.
 */

public class TrainingConfirmationDialogFragment extends DialogFragment {

    protected String item;
    DialogInterface mInterface;
    TrainingConfirmationDialogFragment oldFragment;
    protected DailyTrainingDto dailyTrainingDto;
    protected TrainingDialogFragment trainingDialogFragment;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */

    public void setContent(DialogInterface mInterface,TrainingConfirmationDialogFragment newFragment ,String item){
        this.mInterface = mInterface;
        this.item = item;
        this.oldFragment = newFragment;
    }

    public void setContent(TrainingDialogFragment trainingDialogFragment, DialogInterface mInterface, DailyTrainingDto dailyTrainingDto, TrainingConfirmationDialogFragment newFragment , String item){
        this.mInterface = mInterface;
        this.dailyTrainingDto = dailyTrainingDto;
        this.item = item;
        this.oldFragment = newFragment;
        this.trainingDialogFragment = trainingDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyCustomDialogInfo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dialog_fragment_training_confirmation, container, false);
        TextView tv = (TextView) v.findViewById(R.id.dialog_text);

        tv.setText(this.item);

        // Watch for button clicks.
        Button buttonConfirm = (Button)v.findViewById(R.id.dialog_button_confirm);
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(trainingDialogFragment != null){
                    mInterface.clickOnConfirmation(oldFragment,trainingDialogFragment,dailyTrainingDto);
                }else{
                    mInterface.clickOnConfirmation(oldFragment);
                }
            }
        });

        Button buttonCancel = (Button)v.findViewById(R.id.dialog_button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return v;
    }

    public void closeDialog(){
        getDialog().dismiss();
    }

}
