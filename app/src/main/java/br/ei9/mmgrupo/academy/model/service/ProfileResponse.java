package br.ei9.mmgrupo.academy.model.service;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by vini on 25/01/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileResponse implements Serializable {

    private int status;

    public ProfileResponse() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
