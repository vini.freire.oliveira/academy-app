package br.ei9.mmgrupo.academy.model.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by vini on 26/01/18.
 */

@RealmClass
public class TrainingDto extends RealmObject {

    @PrimaryKey
    protected Integer id;

    protected String coach;
    protected String group;
    protected String turn;
    protected Date date;
    protected String equipament;
    protected String exercise;
    protected String number;
    protected String repetition;
    protected String muscleType;
    protected String obs;

    public TrainingDto() {
    }

    public TrainingDto(Integer id, String coach, String group, String turn, Date date, String equipament, String exercise, String number, String repetition, String muscleType, String obs) {
        this.id = id;
        this.coach = coach;
        this.group = group;
        this.turn = turn;
        this.date = date;
        this.equipament = equipament;
        this.exercise = exercise;
        this.number = number;
        this.repetition = repetition;
        this.muscleType = muscleType;
        this.obs = obs;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getTurn() {
        return turn;
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEquipament() {
        return equipament;
    }

    public void setEquipament(String equipament) {
        this.equipament = equipament;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRepetition() {
        return repetition;
    }

    public void setRepetition(String repetition) {
        this.repetition = repetition;
    }

    public String getMuscleType() {
        return muscleType;
    }

    public void setMuscleType(String muscleType) {
        this.muscleType = muscleType;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }
}
