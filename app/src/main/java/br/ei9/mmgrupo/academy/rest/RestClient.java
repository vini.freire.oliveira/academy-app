package br.ei9.mmgrupo.academy.rest;

import org.androidannotations.annotations.rest.*;
import org.androidannotations.api.rest.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import br.ei9.mmgrupo.academy.model.service.Login;

/**
 * Created by vinif_000 on 12/08/2017.
 */

@Rest(rootUrl = "https://academy-system.herokuapp.com/api_application/",
        converters = {MappingJackson2HttpMessageConverter.class, StringHttpMessageConverter.class},
        interceptors = {InterceptorRest.class})
public interface RestClient {

    @Post("mobile/auth/sign_in")
    @Accept(MediaType.APPLICATION_JSON)
    @RequiresHeader({"Content-Type"})
    void login(Login obj);

    @Get("mobile/auth/validate_token")
    @Accept(MediaType.APPLICATION_JSON)
    @RequiresHeader({"Content-Type","uid","client","access-token"})
    void validateToken();

    @Get("mobile/mobile_profile")
    @Accept(MediaType.APPLICATION_JSON)
    @RequiresHeader({"Content-Type","uid","client","access-token"})
    void getMobileProfile();

    @Get("mobile/profile_payments")
    @Accept(MediaType.APPLICATION_JSON)
    @RequiresHeader({"Content-Type","uid","client","access-token"})
    void getProfilePayments();

    @Get("mobile/profile_measures")
    @Accept(MediaType.APPLICATION_JSON)
    @RequiresHeader({"Content-Type","uid","client","access-token"})
    void getResponse();

    @Get("mobile/profile_training")
    @Accept(MediaType.APPLICATION_JSON)
    @RequiresHeader({"Content-Type","uid","client","access-token"})
    void getCardResponse();

    void setHeader(String name, String value);

}
