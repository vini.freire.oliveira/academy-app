package br.ei9.mmgrupo.academy.view.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.ei9.mmgrupo.academy.R;
import br.ei9.mmgrupo.academy.database.RealmObjectManipulate;
import br.ei9.mmgrupo.academy.model.realm.GoalsDto;
import br.ei9.mmgrupo.academy.model.realm.MeasuresDto;
import br.ei9.mmgrupo.academy.view.interfaces.DialogInterface;

/**
 * Created by vini on 28/02/18.
 */

public class GoalsDialogFragment extends DialogFragment {
    protected RadioButton missLow;
    protected RadioButton missMedium;
    protected RadioButton missHigh;
    protected RadioButton gainLow;
    protected RadioButton gainMedium;
    protected RadioButton gainHigh;
    protected DialogInterface dialogInterface;
    protected GoalsDialogFragment oldFragment;

    public void setDialogInterface(DialogInterface dialogInterface, GoalsDialogFragment oldFragment) {
        this.dialogInterface = dialogInterface;
        this.oldFragment = oldFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyCustomDialogInfo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dialog_fragment_goals, container, false);
        missLow = (RadioButton) v.findViewById(R.id.miss_low);
        missMedium = (RadioButton) v.findViewById(R.id.miss_medium);
        missHigh = (RadioButton) v.findViewById(R.id.miss_high);
        gainLow = (RadioButton) v.findViewById(R.id.gain_low);
        gainMedium = (RadioButton) v.findViewById(R.id.gain_medium);
        gainHigh = (RadioButton) v.findViewById(R.id.gain_high);

        final List<MeasuresDto> measuresDtos = (List<MeasuresDto>) RealmObjectManipulate.getAll(MeasuresDto.class);
        final Float weight = measuresDtos.get(measuresDtos.size() - 1).getWeight();
        final Float low = (weight*2)/100;
        final Float medium = (weight*3.5f)/100;
        final Float high = (weight*5)/100;

        missLow.setText("Perder " +low+ "kg");
        missMedium.setText("Perder " +medium+ "kg");
        missHigh.setText("Perder " +high+ "kg");
        gainLow.setText("Ganhar " +low+ "kg");
        gainMedium.setText("Ganhar " +medium+ "kg");
        gainHigh.setText("Ganhar " +high+ "kg");

        // Watch for button clicks.
        Button buttonConfirm = (Button)v.findViewById(R.id.dialog_button_confirm);
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoalsDto goalsDto = new GoalsDto();
                goalsDto.setId(measuresDtos.size());
                goalsDto.setDate(new Date());
                Date dateGoals = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(dateGoals);
                c.add(Calendar.MONTH, 1);
                goalsDto.setDateGoal(c.getTime());
                if(missLow.isChecked()){
                    goalsDto.setWeightGoal(weight - low);
                }else if(missMedium.isChecked()){
                    goalsDto.setWeightGoal(weight - medium);
                }else if(missHigh.isChecked()){
                    goalsDto.setWeightGoal(weight - high);
                }else if(gainLow.isChecked()){
                    goalsDto.setWeightGoal(weight + low);
                }else if(gainMedium.isChecked()){
                    goalsDto.setWeightGoal(weight + low);
                }else if(gainHigh.isChecked()){
                    goalsDto.setWeightGoal(weight + low);
                }
                dialogInterface.clickOnConfirmation(oldFragment,goalsDto);
            }
        });

        Button buttonCancel = (Button)v.findViewById(R.id.dialog_button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return v;
    }
}
