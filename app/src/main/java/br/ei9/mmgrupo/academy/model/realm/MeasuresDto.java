package br.ei9.mmgrupo.academy.model.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by vini on 25/01/18.
 */

@RealmClass
public class MeasuresDto extends RealmObject {

    @PrimaryKey
    protected Integer id;

    protected Float weight;
    protected Float height;
    protected Float waist;
    protected Float pectoral;
    protected Float hip;
    protected Float armRight;
    protected Float armLeft;
    protected Float thighRight;
    protected Float thighLeft;
    protected Float calfRight;
    protected Float calfLeft;
    protected Date date;

    public MeasuresDto() {
    }

    public MeasuresDto(Integer id, Float weight, Float height, Float waist, Float pectoral, Float hip, Float armRight, Float armLeft, Float thighRight, Float thighLeft, Float calfRight, Float calfLeft, Date date) {
        this.id = id;
        this.weight = weight;
        this.height = height;
        this.waist = waist;
        this.pectoral = pectoral;
        this.hip = hip;
        this.armRight = armRight;
        this.armLeft = armLeft;
        this.thighRight = thighRight;
        this.thighLeft = thighLeft;
        this.calfRight = calfRight;
        this.calfLeft = calfLeft;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWaist() {
        return waist;
    }

    public void setWaist(Float waist) {
        this.waist = waist;
    }

    public Float getPectoral() {
        return pectoral;
    }

    public void setPectoral(Float pectoral) {
        this.pectoral = pectoral;
    }

    public Float getHip() {
        return hip;
    }

    public void setHip(Float hip) {
        this.hip = hip;
    }

    public Float getArmRight() {
        return armRight;
    }

    public void setArmRight(Float armRight) {
        this.armRight = armRight;
    }

    public Float getArmLeft() {
        return armLeft;
    }

    public void setArmLeft(Float armLeft) {
        this.armLeft = armLeft;
    }

    public Float getThighRight() {
        return thighRight;
    }

    public void setThighRight(Float thighRight) {
        this.thighRight = thighRight;
    }

    public Float getThighLeft() {
        return thighLeft;
    }

    public void setThighLeft(Float thighLeft) {
        this.thighLeft = thighLeft;
    }

    public Float getCalfRight() {
        return calfRight;
    }

    public void setCalfRight(Float calfRight) {
        this.calfRight = calfRight;
    }

    public Float getCalfLeft() {
        return calfLeft;
    }

    public void setCalfLeft(Float calfLeft) {
        this.calfLeft = calfLeft;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
