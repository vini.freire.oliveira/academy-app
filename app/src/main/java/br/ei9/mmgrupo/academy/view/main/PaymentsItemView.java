package br.ei9.mmgrupo.academy.view.main;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;

import br.ei9.mmgrupo.academy.R;

/**
 * Created by vini on 24/01/18.
 */

@EViewGroup(R.layout.item_view_payment_day)
public class PaymentsItemView extends LinearLayout {

    @ViewById(R.id.payment_data)
    TextView data;

    @ViewById(R.id.payment_value)
    TextView value;

    public PaymentsItemView(Context context) {
        super(context);
    }

    public void bind(PaymentsItem item){
        value.setText(item.getValue() + " " +getResources().getString(R.string.coins));
        data.setText( new SimpleDateFormat("dd/MM/yyyy").format(item.getDate()));
    }
}
