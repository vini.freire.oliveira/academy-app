package br.ei9.mmgrupo.academy.view.main;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vini on 24/01/18.
 */

@EBean
public class PaymentsAdapter extends BaseAdapter {

    List<PaymentsItem> objects;

    @RootContext
    Context context;

    @AfterInject
    public void inject(){
        this.objects = populate();
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public PaymentsItem getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PaymentsItemView view;

        if(convertView == null){
            view = PaymentsItemView_.build(context);
        }else{
            view = (PaymentsItemView) convertView;
        }

        view.bind(objects.get(position));

        return view;
    }

    protected List<PaymentsItem> populate(){
        return new ArrayList<>();
    }

    public void setList(PaymentsItem item){
        objects.add(item);
    }

    public void setList(List<PaymentsItem> objects){
        this.objects = objects;
    }
}
