package br.ei9.mmgrupo.academy.view.main;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import br.ei9.mmgrupo.academy.R;

/**
 * Created by vini on 26/01/18.
 */

public class TrainingExpandableListAdapter extends BaseExpandableListAdapter {

    protected Context _context;
    protected List<TrainingHeaderItem> _listDataHeader;
    protected HashMap<TrainingHeaderItem,List<TrainingChildItem>> _listDataChild;

    public TrainingExpandableListAdapter(Context _context, List<TrainingHeaderItem> listDataHeader, HashMap<TrainingHeaderItem, List<TrainingChildItem>> ListChildData) {
        this._context = _context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = ListChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        TrainingChildItem item = _listDataHeader.get(groupPosition).getItens().get(childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_header_training, null);
        }

        TextView muscleTextView = (TextView) convertView.findViewById(R.id.muscle);
        TextView equipamentTextView = (TextView) convertView.findViewById(R.id.equipament);
        TextView exerciseTextView = (TextView) convertView.findViewById(R.id.exercise);
        TextView numberSerieTextView = (TextView) convertView.findViewById(R.id.number_serie);

        muscleTextView.setText(item.getMuscleType());
        equipamentTextView.setText(item.getEquipament());
        exerciseTextView.setText(item.getExercise());
        numberSerieTextView.setText(item.getNumber() + "x" + item.getRepetition());

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        TrainingHeaderItem item = _listDataHeader.get(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.header_training_list, null);
        }

        TextView group = (TextView) convertView.findViewById(R.id.group);
        group.setTypeface(null, Typeface.BOLD);
        group.setText(item.getGroup());
        TextView turn = (TextView) convertView.findViewById(R.id.turn);
        turn.setText(item.getTurn());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public HashMap<TrainingHeaderItem, List<TrainingChildItem>> getlistDataChild() {
        return _listDataChild;
    }

}
